<!doctype html>
<html lang="en">

<head>
  <link rel="stylesheet" type="text/css" href="css/email.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Email Inquiry</title>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark transparente mb-4">
        <a class="navbar-brand" href="#">Email Inquiry</a>
    </nav>
    <form action="send_mail.php" id="form1" method="post">
      <div id="form-main">
        <div id="form-div">
          <form class="form" id="form1">

            <p class="name">
              <input name="email" type="text" class="length[0,100] feedback-input" placeholder="Email" id="email" required />
            </p>
            <p class="email">
              <input name="subject" type="text" class="length[0,100] feedback-input" id="subject" placeholder="Subject"requried/>
            </p>
            <p class="text">
              <textarea name="message" class="length[6,300] feedback-input" id="Message" placeholder="Message"></textarea>
            </p>
            <div class="submit">
              <input type="submit" value="SEND" id="button-blue"/>
              <div class="ease"></div>
            </div>
          </form>
        </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
